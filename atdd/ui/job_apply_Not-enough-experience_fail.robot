**Settings**
Library    SeleniumLibrary

***Variables***


***Test Cases***
คุณวรวิทย์ค้นหางาน Programmer และสมัครงานสำเร็จ
    เข้าเว๊ปไซต์หน้าค้นหา
    พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    หน้าเว๊ปจะขึ้นอาชีพ Programmer 4 ตัว 
    เลือกตำแหน่งงาน Junior Java Programmer
    ถ้าประสบการณ์การทำงานเปลี่ยนเป็น 2
    กดสมัคร
    ระบบแสดงผลลัพธ์ว่า สมัครไม่สำเร็จ
    
    

**Keywords***
เข้าเว๊ปไซต์หน้าค้นหา
    open Browser    http://localhost:3000/searchJob    chrome
    set Selenium Speed    0.5

พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว๊ปจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_3    Junior Java Programmer

เลือกตำแหน่งงาน Junior Java Programmer
    Click Element    id=show_detail_3
    Wait Until Element Contains    id=name    Junior Java Programmer

ถ้าประสบการณ์การทำงานเปลี่ยนเป็น 2
    Press Keys    id=resume_experience    CTRL+a+BACKSPACE
    Input Text    id=resume_experience    2

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพธ์ว่า สมัครไม่สำเร็จ
    Element Should Contain    id=message    Not enough experience
