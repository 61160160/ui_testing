**Settings**
Library    SeleniumLibrary

***Variables***


***Test Cases***
คุณวรวิทย์ค้นหางาน Programmer และสมัครงานสำเร็จ
    เข้าเว๊ปไซต์หน้าค้นหา
    พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    หน้าเว๊ปจะขึ้นอาชีพ Programmer 4 ตัว 
    เลือกตำแหน่งงาน Senior Java Programmer
    ถ้าอายุเปลี่ยนเป็น 24
    กดสมัคร
    ระบบแสดงผลลัพธ์ว่า สมัครไม่สำเร็จ
    
    

**Keywords***
เข้าเว๊ปไซต์หน้าค้นหา
    open Browser    http://localhost:3000/searchJob    chrome
    set Selenium Speed    0.5

พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว๊ปจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_2    Senior Java Programmer

เลือกตำแหน่งงาน Senior Java Programmer
    Click Element    id=show_detail_2
    Wait Until Element Contains    id=name    Senior Java Programmer

ถ้าอายุเปลี่ยนเป็น 24
    Press Keys    id=resume_age    CTRL+a+BACKSPACE
    Input Text    id=resume_age    24

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพธ์ว่า สมัครไม่สำเร็จ
    Element Should Contain    id=message    Age out of range
